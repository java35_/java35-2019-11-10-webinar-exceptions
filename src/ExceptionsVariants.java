import java.io.Reader;
import java.io.StringReader;
import java.util.Scanner;

public class ExceptionsVariants {
	public static void main(String[] args) {
		int array[] = {1, 2, 3, 4, 5};
		
//		System.out.print(array[10]);
		try {
			System.out.print(array[10]);
		} catch (Exception e) {
			System.out.println("!!!");
//			e.printStackTrace();
		}
		
//		System.out.println(5 / 0);
		try {
			System.out.println(5 / 0);
		} catch (Exception e) {
			System.out.println("�� ���� ������ ������");
		}
		
		Scanner scaner = new Scanner(System.in);
		System.out.println("Input value 1: ");
		System.out.print("> ");
		int value1 = scaner.nextInt();
		System.out.println("Input value 2: ");
		System.out.print("> ");
		int value2 = scaner.nextInt();
		try {
			System.out.println(value1 + " / " + value2 + " = " + (value1 / value2));
		} catch (ArithmeticException e) {
			System.out.println("Invalid value(s)");
		} finally {
			System.out.println("����������� ������");
		}
	}
}
