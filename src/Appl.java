import java.io.FileInputStream;

public class Appl {
	public static void main(String[] args) {
		long start;
		long end;
		int sum;
		int array[] = new int[1_00];
		
		for (int i = 0; i < array.length; i++) {
			array[i] = i;
		}
		
		sum = 0;
		start = System.nanoTime();
		for (int i = 0; i < array.length; i++) {
			sum += array[i];
		}
		end = System.nanoTime();
		System.out.println(end - start);
		
		sum = 0;
		int i = 0;
		start = System.nanoTime();
		try {
			while(true) {
				sum += array[i++];
			}
		} catch (Exception e) {
			end = System.nanoTime();
			System.out.println(end - start);
		}
		
	}
}
